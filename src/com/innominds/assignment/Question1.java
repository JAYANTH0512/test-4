package com.innominds.assignment;

class Thread1 extends Thread {
	public void run() {
		for (int i = 0; i < 3; i++) {
			System.out.println("Good Morning");
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}
class Thread2 extends Thread{
	public void run() {
		for (int i = 0; i < 3; i++) {
			System.out.println("Hi");
			try {
				Thread.sleep(2000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
}	
class Thread3 extends Thread{
	public void run() {
		for (int i = 0; i < 3; i++) {
			System.out.println("Welcome");
			try {
				Thread.sleep(3000);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}	
 }	


public class Question1 {

	public static void main(String[] args) {

		Thread1 t1 = new Thread1();
		Thread2 t2 = new Thread2();
		Thread3 t3 = new Thread3();
		
	
		t1.setName("one");
		t2.setName("two");
		t3.setName("three");
		
		System.out.println(t1);
		System.out.println(t2);
		System.out.println(t3);
		
		t1.start();
		t2.start();
		t3.start();	
		
	}	
	}
