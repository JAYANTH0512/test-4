package com.innominds.assignment;


class Good implements Runnable{

	@Override
	public void run() {
		for (int i = 0; i < 15; i++) {
			System.out.println("Good Morning");
			try {
				Thread.sleep(1000);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		
	}
}

class Hi implements Runnable{

	@Override
	public void run() {
		for (int i = 0; i < 15; i++) {
			System.out.println("Hii");
			try {
				Thread.sleep(2000);
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}		
	}	
}
class Welcome implements Runnable{

	@Override
	public void run() {
		for (int i = 0; i < 15; i++) {
			System.out.println("Welcome");
			try {
				Thread.sleep(3000);
			} catch (Exception e) {
				e.printStackTrace();
			}			
		}		
	}	
}

public class Question{
	public static void main(String[] args) {
		Runnable obj1 = new Good();
		Runnable obj2 = new Hi();
		Runnable obj3 = new Welcome();
		
		Thread t1 = new Thread(obj1);
		Thread t2 = new Thread(obj2);
		Thread t3 = new Thread(obj3);
		
		t1.start();
		try {Thread.sleep(500);}catch (Exception e) {};
		t2.start();
		try {Thread.sleep(500);}catch (Exception e) {};
		t3.start();	
	}
}

