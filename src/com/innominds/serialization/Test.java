package com.innominds.serialization;

//6.Write a program of object serialization using transient keyword

/**
 * Java Program to perform serialization
 */

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

class Test implements Serializable
{
    // Normal variables
    int i = 1, j = 2;
  
    // Transient variables
    transient int k = 30;
  

  
    public static void main(String[] args) throws Exception
    {
        Test input = new Test();
  
        // serialization
        FileOutputStream file = new FileOutputStream("xyz.txt");
        ObjectOutputStream oos = new ObjectOutputStream(file);
        oos.writeObject(input);
  
        // de-serialization
        FileInputStream fis = new FileInputStream("xyz.txt");
        ObjectInputStream obj = new ObjectInputStream(fis);
        Test output = (Test)obj.readObject();
        System.out.println("i = " + output.i);
        System.out.println("j = " + output.j);
        System.out.println("k = " + output.k);
    }
}